from django.forms import ModelForm
from django import forms

from receipts.models import (
    Receipt,
    ExpenseCategory,
    Account,
)


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]
        widgets = {
            "date": forms.widgets.SelectDateWidget(attrs={"type": "datetime"})
        }

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
